.. Stochastic processes and Data mining with Python documentation master file, created by
   sphinx-quickstart on Sat Mar  4 18:02:21 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Stochastic processes and Data mining with Python
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   datamining/index


Indices and tables
==================

* :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
